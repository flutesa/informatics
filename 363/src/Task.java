import java.util.Scanner;


public class Task {

	public static void reverse(int array[], int start, int stop) {
		int i = start;
		int j = stop-1;
		int[] a = array;
		int buffer;
		
		while (i<=j) {
			buffer = a[i];
			a[i] = a[j];
			a[j] = buffer;
			i += 1;
			j -= 1;
		}
	}
	
	public static void reverse_v2(int array[][], int str, int start, int stop) {
		int i = start;
		int j = stop-1;
		int[][] a = array;
		int buffer;
		
		while (i<=j) {
			buffer = a[str][i];
			a[str][i] = a[str][j];
			a[str][j] = buffer;
			i += 1;
			j -= 1;
		}
	}
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	    int a=sc.nextInt();
	    int b=sc.nextInt();
	    sc.close();
		
	    int[][] array = new int[a][b];
	    int i;
	    int j;
	    int counter = 0;
	    
	    for (i=0; i<a; i++) {
	    	for (j=0; j<b; j++) {
	    		array[i][j] = counter;
	    		counter++;
	    	}
	    }
   
	    for (i=0; i<a; i++) {
	    	if (i % 2 != 0) {
	    		reverse_v2(array, i, 0, b);
	    	}	    	
	    }

	    
	    for (i=0; i<a; i++) {
	    	for (j=0; j<b; j++) {
	    		if (j == (b-1)) {
		    		if (array[i][j] < 10) System.out.println("  " + array[i][j]);
		    		else if ((array[i][j] < 100) || (array[i][j] >= 10)) System.out.println(" " + array[i][j]);
		    		else if ((array[i][j] >= 100) || (array[i][j] < 1000)) System.out.println(array[i][j]);
	    		}
	    		
	    		else if (array[i][j] < 10) System.out.print("  " + array[i][j]);
	    		else if ((array[i][j] < 100) || (array[i][j] >= 10)) System.out.print(" " + array[i][j]);
	    		else if ((array[i][j] >= 100) || (array[i][j] < 1000)) System.out.print(array[i][j]);
	    	}
	    }
	
	}

}
